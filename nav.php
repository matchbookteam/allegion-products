<div class="color-wrapper-1 landing heading-blue">
    <div class="fixed-area affix">
        <header>












            <nav class="header-navigation">
                <ul class="navigation-list">

                    <li class="navigation-item"><a href="/corp/en/header/career-nav.html">Careers</a></li>

                    <li class="navigation-item"><a href="/corp/en/header/media.html">Media</a></li>

                    <li class="navigation-item"><a href="/corp/en/header/locations.html">Locations</a></li>

                    <li class="navigation-item"><a href="/corp/en/header/contact-us.html">Contact Us</a></li>


                    <!--adding worldwide sites-->
                    <li class="dropdown navigation-item">
                        <a href="#" role="button" id="drop1" class="paddingclass">
                            <img src="http://allegion.com/etc/designs/allegionCorp2/img/allegion-icon-globe-black.png" style="position:absolute;top:0; left:0">
                            Worldwide Sites
                            <img style="position:absolute;top:10px; left:130px" src="http://allegion.com/etc/designs/allegionCorp2/img/nav-down-arrow.gif">
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="drop1" role="menu">

                            <li class="liDropDown"><a href="/corp/en/worldwide/USA.html">USA</a></li>

                            <li class="liDropDown"><a href="/corp/en/worldwide/EMEIA.html">EMEIA</a></li>

                            <li class="liDropDown"><a href="/corp/en/worldwide/Australia.html">Australia</a></li>

                            <li class="liDropDown"><a href="/corp/en/worldwide/new_zealand.html">New Zealand</a></li>

                            <li class="liDropDown"><a href="/corp/en/worldwide/china.html">China</a></li>

                        </ul>
                    </li>
                    <!--end-->

                    <li class="navigation-item has-dropdown">
                        NYSE
                    </li>
                    <li class="navigation-item has-dropdown">
                        ALLE
                    </li>
                    <li class="navigation-item has-dropdown">
                        <span id="stockprice">67.43</span>
                    </li>

                    <li class="navigation-item has-dropdown">

                        <span style="font-size: 16px;" id="stockDirection" class="green">⇧</span>&nbsp;&nbsp;<span id="stockChange" class="green">0.09</span>
                    </li>


                    <script type="text/javascript">

                        $.ajax({
                            type: 'POST',
                            url: '/bin/allegionCorp2/readStockDetails',
                            success: function (msg) {
                                var xmlText = msg;
                                //alert('xmlText = '+xmlText);
                                var xmlDOM = $.parseXML(xmlText);
                                var price = $(xmlDOM).find('CurrentPrice').text();
                                var change = $(xmlDOM).find('Change').text();
                                $('#stockprice').text(price);
                                $('#stockChange').text(Math.abs(change));
                                if (change > 0) {
                                    $('#stockDirection').html('&#x21e7;');
                                    $('#stockDirection').addClass("green");
                                    $('#stockChange').addClass("green");
                                } else {
                                    $('#stockDirection').html('&#x21e9;');
                                    $('#stockDirection').addClass("red");
                                    $('#stockChange').addClass("red");
                                }

                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                //alert('inside error');
                            }
                        });
                    </script>

                </ul></nav></header>
        <div class="row header-wrapper">
            <!-- Logo -->
            <div class="col-xs-9 col-sm-6 logo">
                <a href="/"><img title="allegion-logo" alt="allegion-logo" class="cq-dd-image" src="http://www.allegion.com/content/allegion-corp/corp/_jcr_content/logos/mainLogo.img.png"></a>
            </div>

            <div id="search_box" class="col-xs-9 col-sm-6 search">
                <form name="searchform" action="/corp/en/search.html" method="get">
                    <input name="q" id="searchInput" onfocus="this.value=''" type="text" size="20" value="Search Allegion.com" title="Search Allegion.com"> <input title="Search" type="submit" value="Search" class="search_btn" id="searchClick" disabled="true">
                </form>
                <script>
                    $('#searchInput').keyup(function() {
                        var empty = false;

                        if ($.trim($(this).val()) == '') {
                            empty = true;
                        }
                        if (empty) {
                            $('#searchClick').attr('disabled', true);
                        } else {
                            $('#searchClick').attr('disabled', false);
                        }
                    });
                </script>
            </div>

            <!-- Main Navigation - mobile -->
            <div class="col-xs-3 col-sm-6 hamburger">
                <a class="mobile-toggle" data-toggle="collapse" href="#accordion" data-parent="#accordion" aria-controls="#accordion" aria-expanded="true">Menu</a>
                <div class="collapse" id="accordion" aria-multiselectable="true" style="max-height: 878px;">

                    <div class="panel">
                        <div class="panel-heading" role="tab" id="heading1">
                            <div class="panel-direct-link">
                                <a href="/corp/en/about.html">▸</a>
                            </div>
                            <div class="panel-title">


                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" aria-controls="#collapse1">
                                    About Allegion
                                </a>



                            </div>
                        </div>


                        <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                            <div class="panel-body">
                                <ul class="list-group">

                                    <li class="list-group-item"><a href="/corp/en/about/our-story.html">Our Story</a></li>

                                    <li class="list-group-item"><a href="/corp/en/about/corporate-governance.html">Corporate Governance</a></li>

                                    <li class="list-group-item"><a href="/corp/en/about/leadership.html">Leadership</a></li>

                                    <li class="list-group-item"><a href="/corp/en/about/expertise.html">Expertise</a></li>

                                </ul>
                            </div>
                        </div>


                    </div>

                    <div class="panel">
                        <div class="panel-heading" role="tab" id="heading2">
                            <div class="panel-direct-link">
                                <a href="/corp/en/products.html">▸</a>
                            </div>
                            <div class="panel-title">


                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="#collapse2">
                                    Products
                                </a>



                            </div>
                        </div>


                        <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                            <div class="panel-body">
                                <ul class="list-group">

                                    <li class="list-group-item"><a href="/corp/en/products/business.html">For Business</a></li>

                                    <li class="list-group-item"><a href="/corp/en/products/home.html">For Home</a></li>

                                </ul>
                            </div>
                        </div>


                    </div>

                    <div class="panel">
                        <div class="panel-heading" role="tab" id="heading3">
                            <div class="panel-direct-link">
                                <a href="/corp/en/brands.html">▸</a>
                            </div>
                            <div class="panel-title">



                                <a href="/corp/en/brands.html" class="collapsed">
                                    Brands
                                </a>


                            </div>
                        </div>



                    </div>

                    <div class="panel">
                        <div class="panel-heading" role="tab" id="heading4">
                            <div class="panel-direct-link">
                                <a href="/corp/en/careers.html">▸</a>
                            </div>
                            <div class="panel-title">


                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="#collapse4">
                                    Careers
                                </a>



                            </div>
                        </div>


                        <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                            <div class="panel-body">
                                <ul class="list-group">

                                    <li class="list-group-item"><a href="/corp/en/careers/culture.html">Our Culture</a></li>

                                    <li class="list-group-item"><a href="/corp/en/careers/people.html">Our People</a></li>

                                    <li class="list-group-item"><a href="/corp/en/careers/university.html">During and After University</a></li>

                                    <li class="list-group-item"><a href="/corp/en/careers/equal-opportunity.html">Equal Opportunity Employer</a></li>

                                </ul>
                            </div>
                        </div>


                    </div>

                    <div class="panel">
                        <div class="panel-heading" role="tab" id="heading5">
                            <div class="panel-direct-link">
                                <a href="/corp/en/investor-relations.html">▸</a>
                            </div>
                            <div class="panel-title">



                                <a href="/corp/en/investor-relations.html" class="collapsed">
                                    Investor Relations
                                </a>


                            </div>
                        </div>



                    </div>

                    <div class="panel">
                        <div class="panel-heading" role="tab" id="heading6">
                            <div class="panel-direct-link">
                                <a href="/corp/en/news-landing-page.html">▸</a>
                            </div>
                            <div class="panel-title">


                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="#collapse6">
                                    Newsroom
                                </a>



                            </div>
                        </div>


                        <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                            <div class="panel-body">
                                <ul class="list-group">

                                    <li class="list-group-item"><a href="/corp/en/news-landing-page/media.html">Media Resources</a></li>

                                    <li class="list-group-item"><a href="/corp/en/news-landing-page/social-media.html">Social Media</a></li>

                                    <li class="list-group-item"><a href="/corp/en/news-landing-page/press-releases-articles.html">Press Releases &amp; Articles</a></li>

                                </ul>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>

        <div class="nav-container container">
            <!-- Main Navigation -->
            <div class="main-navigation" role="navigation">
                <ul class="nav nav-justified">

                    <li class="dropdown" data-child=".child-1">
                        <a href="/corp/en/about.html" role="button">About Allegion</a>
                    </li>

                    <li class="dropdown" data-child=".child-2">
                        <a href="/corp/en/products.html" role="button">Products</a>
                    </li>

                    <li class="dropdown" data-child=".child-3">
                        <a href="/corp/en/brands.html" role="button">Brands</a>
                    </li>

                    <li class="dropdown" data-child=".child-4">
                        <a href="/corp/en/careers.html" role="button">Careers</a>
                    </li>

                    <li class="dropdown" data-child=".child-5">
                        <a href="/corp/en/investor-relations.html" role="button">Investor Relations</a>
                    </li>

                    <li class="dropdown" data-child=".child-6">
                        <a href="/corp/en/news-landing-page.html" role="button">Newsroom</a>
                    </li>

                </ul>
            </div>

            <!--Children of main Navigation-->
            <div class="child-nav" role="navigation">

                <ul class="child-1 nav nav-justified" style="display: none;">

                    <li>
                        <a href="/corp/en/about/our-story.html" role="button" aria-expanded="false">Our Story</a>
                    </li>

                    <li>
                        <a href="/corp/en/about/corporate-governance.html" role="button" aria-expanded="false">Corporate Governance</a>
                    </li>

                    <li>
                        <a href="/corp/en/about/leadership.html" role="button" aria-expanded="false">Leadership</a>
                    </li>

                    <li>
                        <a href="/corp/en/about/expertise.html" role="button" aria-expanded="false">Expertise</a>
                    </li>

                </ul>

                <ul class="child-2 nav nav-justified" style="display: block;">

                    <li>
                        <a href="/corp/en/products/business.html" role="button" aria-expanded="false">For Business</a>
                    </li>

                    <li>
                        <a href="/corp/en/products/home.html" role="button" aria-expanded="false">For Home</a>
                    </li>

                </ul>

                <ul class="child-3 nav nav-justified" style="display: none;">

                </ul>

                <ul class="child-4 nav nav-justified" style="display: none;">

                    <li>
                        <a href="/corp/en/careers/culture.html" role="button" aria-expanded="false">Our Culture</a>
                    </li>

                    <li>
                        <a href="/corp/en/careers/people.html" role="button" aria-expanded="false">Our People</a>
                    </li>

                    <li>
                        <a href="/corp/en/careers/university.html" role="button" aria-expanded="false">During and After University</a>
                    </li>

                    <li>
                        <a href="/corp/en/careers/equal-opportunity.html" role="button" aria-expanded="false">Equal Opportunity Employer</a>
                    </li>

                </ul>

                <ul class="child-5 nav nav-justified" style="display: none;">

                </ul>

                <ul class="child-6 nav nav-justified" style="display: none;">

                    <li>
                        <a href="/corp/en/news-landing-page/media.html" role="button" aria-expanded="false">Media Resources</a>
                    </li>

                    <li>
                        <a href="/corp/en/news-landing-page/social-media.html" role="button" aria-expanded="false">Social Media</a>
                    </li>

                    <li>
                        <a href="/corp/en/news-landing-page/press-releases-articles.html" role="button" aria-expanded="false">Press Releases &amp; Articles</a>
                    </li>

                </ul>

            </div>
        </div>
    </div>





</div>