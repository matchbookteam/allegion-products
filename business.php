

<div class="container" id="body-section">
	<div class="row">
		<div class="col-xs-12 icons selection-icons searchbox">
			<div class="float-container">
				<div class="sort-item select-me">
					<a href="business-step3.html" ><img src="assets/icon.png" />Product Type A</a>
				</div>
				<div class="sort-item">
					<img src="assets/Ellipse.png"/>Product Type B
				</div>
				<div class="sort-item">
					<img src="assets/trangle.png"/>Product Type C
				</div>
				<div class="sort-item">
					<img src="assets/icon.png"/>Product Type D
				</div>
				<div class="sort-item">
					<img src="assets/Ellipse.png"/>Product Type E
				</div>
				<div class="sort-item">
					<img src="assets/trangle.png"/>Product Type F
				</div>
			</div>

		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<table class="table">
				<thead>
				<tr>
					<th width="25%">Brand</th>
					<th width="50%">Description</th>
					<th>Offerings</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<td><img src="assets/Bricard.jpg"/> </td>
					<td>Bricard has a rich history that began in 1782 and has continued to grow into one of the most trusted security brands. With solutions for both home and industry safety, French-based Bricard is a market leader in locks and access control systems.</td>
					<td class="icons"><img src="assets/icon.png"/><img src="assets/Ellipse.png"/> </td>
				</tr>
				<tr>
					<td><img src="assets/schlage.jpg"/> </td>
					<td>For more than 90 years, Schlage has been creating the strongest and most technologically advanced security products for homes, multi-family, commercial, and institutional buildings. We have been meticulous designers, painstaking engineers, and proud craftsmen.</td>
					<td class="icons"><img src="assets/icon.png"/><img src="assets/Ellipse.png"/><img
							src="assets/trangle.png"/> </td>
				</tr>
				<tr>
					<td><img src="assets/cisa.jpg"/> </td>
					<td>Established in 1926, CISA® is known for producing and patenting the first electrically controlled lock, which dramatically improved the opening of doors and gates. It is also the first brand in the world to develop smart-card locks, which are used as electronic keys to safely and efficiently manage gates and entrances.</td>
					<td class="icons"><img src="assets/icon.png"/></td>
				</tr>
				<tr>
					<td><img src="assets/interflex.jpg"/> </td>
					<td>Interflex Datensysteme, offers complete solutions for workforce management with time accounting and personnel scheduling as well as security systems including card production, CCTV video surveillance, offline components and biometrics. Furthermore, the company supports customer solutions with sustainable and comprehensive consulting services for workforce productivity.</td>
					<td class="icons"><img src="assets/Ellipse.png"/> </td>
				</tr>
				<tr>
					<td><img src="assets/brand.jpg"/> </td>
					<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu risus mollis, lobortis nunc ut, eleifend ipsum. Nullam cursus arcu felis. Aliquam magna nunc, auctor sed nisl cursus, aliquet pulvinar metus.</td>
					<td class="icons"><img src="assets/icon.png"/><img src="assets/Ellipse.png"/><img
							src="assets/trangle.png"/>
					</td>
				</tr>
				<tr>
					<td><img src="assets/brand.jpg"/> </td>
					<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu risus mollis, lobortis nunc ut, eleifend ipsum. Nullam cursus arcu felis. Aliquam magna nunc, auctor sed nisl cursus, aliquet pulvinar metus.</td>
					<td class="icons"><img src="assets/icon.png"/><img
							src="assets/trangle.png"/>
					</td>
				</tr>
				<tr>
					<td><img src="assets/brand.jpg"/> </td>
					<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu risus mollis, lobortis nunc ut, eleifend ipsum. Nullam cursus arcu felis. Aliquam magna nunc, auctor sed nisl cursus, aliquet pulvinar metus.</td>
					<td class="icons"><img src="assets/Ellipse.png"/><img
							src="assets/trangle.png"/>
					</td>
				</tr>
				</tbody>
			</table>
		</div>

	</div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>