<?php
include ('jsonImport.php');
include ('header.php');
include ('billboard.php');

function summary($str, $limit=100, $strip = false) {
	$str = ($strip == true)?strip_tags($str):$str;
	if (strlen ($str) > $limit) {
		$str = substr ($str, 0, $limit - 3);
		return (substr ($str, 0, strrpos ($str, ' ')).'...');
	}
	return trim($str);
}
function typeCheck($type){
    if ($type['url']){
        $class = 'visible';
    }

    return $class;
}
?>
<div class="parbase multipleColumns section">
	<div class="content-container no-margin row">
<!--		Icon Key Section Start-->
		<div  class="icon-key ">
			<div class="icon-key-header">
                <div class="row">
                    <div class="col-xs-8">
                        <h2>Product Icon Key</h2>
                    </div>
                    <div id="layout-selectors" class="col-xs-4">
                        <i id="list-view-selector" class="fa fa-th-list" aria-hidden="true"></i>
                        <i id="grid-view-selector" class="fa fa-th-large" aria-hidden="true"></i>

                    </div>
                </div>
            </div>
			<div class="icon-key-body">
				<div class="row">
					<?php foreach ($productTypes['productTypes'] as $types) { ?>

                    <div id="<?php echo $types['name'];?>" class="col-xs-4 col-sm-2 col-md-2 selector">
						<div class="row">
							<div class="col-xs-5">
								<img src="assets/Product_Icon_<?php echo $types['icon'];?>.png" class="img-responsive">
							</div>
						</div>
						<h2><?php echo $types['title'];?></h2>
						<p><?php echo $types['description'];?></p>
					</div>
					<?php
					}
					?>

				</div>
			</div>
		</div>
<!--		Icon Key Section Stop-->
<!--		Brand and Offering List Start-->
        <div id="brands">
            <div id="active-brands" class="row">
            <div class="brandlisting brandlisting-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <h3>Brand</h3>
                    </div>
                    <div class="col-xs-5"><h3>Description</h3></div>
                    <div class="col-xs-4 row"><h3>Offerings</h3>
                    </div>

                </div>
            </div>


	<?php
	$selected = 'other';
	foreach ($brands['brands'] as $brandVal){

		//Sample variable for selected items from sort above

        //redraw listing area based on sort option
//        print_r($brandVal);

	    if(($brandVal["logoUrl"]) && ($brandVal["description"])){
		    $descriptionShort = summary($brandVal["description"], 300);

?>
        <!-- list view brand item -->
			<div class="brandlisting <?php echo $activeListing;?>">
                <div class="row">
                    <div class="col-xs-3 brandlisting-image" >
                        <a href="<?php echo $brandVal['main'][0]['url']?>" target="_blank"><img src="<?php echo $brandVal['logoUrl'];?>" class="img-responsive "></a>
                    </div>
                    <div class="col-xs-5"><p><?php echo $descriptionShort;?></p></div>
                    <div class="col-xs-4 row">
                        <?php foreach ($productTypes['productTypes'] as $types) {

                            if ($brandVal[$types['name']][0]['url']){$visibility = 'visible';}else{ $visibility = 'hidden';}
                            ?>
                            <div data-toggle="tooltip" title="<?php echo $types['title'];?>" class="icons <?php echo $visibility;?>">
                            <div class="col-xs-4 col-sm-3 col-md-2"><a href="<?php echo $brandVal[$types['name']][0]['url']?>" target="_blank"> <img src="assets/Product_Icon_<?php echo
				$types['icon'];?>.png" class="img-responsive " /></a>

                            </div>
                            </div>

                        <?php
							}?>
                    </div>

				</div>
			</div>
        <!-- list view -->
        <!-- gallery view -->
        <div class="brandlisting-gallery-container col-sm-6 col-md-4 col-lg-4 col-xs-12 <?php echo $activeListing;?>">
            <div class="brandlisting-gallery">
            <div class="row "  >
                <div class="col-xs-12 brandlisting-image">
                    <a href="<?php echo $brandVal['main'][0]['url'];?>" target="_blank"><img src="<?php echo $brandVal['logoUrl'];?>" class="img-responsive "></a>
                </div>
            </div>
            <div class="row brandlisting-gallery-description">
                <div class="col-xs-12">
                    <p class="description-limit"><?php echo $descriptionShort;?></p>
                </div>
            </div>
            <div class="row brandlisting-gallery-icons">
            <?php
            foreach ($productTypes['productTypes'] as $types) {
                if ($brandVal[$types['name']][0]['url']){$visibility = 'visible';}else{ $visibility = 'hidden';}
            ?>
                    <div data-toggle="tooltip" title="<?php echo $types['title'];?>" class="icons <?php echo $visibility;?>">
                        <div class="col-xs-2 col-sm-2 col-md-2">
                            <a href="<?php echo $brandVal[$types['name']][0]['url']?>" target="_blank"> <img src="assets/Product_Icon_<?php echo
		                        $types['icon'];?>.png" class="img-responsive " /></a>
                        </div>

                    </div>

            <?php
            }?>
            </div>
            </div>

        </div>
        <!-- gallery view -->

		<?php
	    }
	    //end product loop
	}
	?>
            </div>
        <div id="inactive-brands" class="row">
            <h2>See Our Other Brands</h2>
            <hr style="border-color:black;"/>
	        <?php
	        $selected = 'other';
	        foreach ($brands['brands'] as $brandVal){

		        //Sample variable for selected items from sort above

		        //redraw listing area based on sort option
//        print_r($brandVal);

		        if(($brandVal["logoUrl"]) && ($brandVal["description"])){
			        $descriptionShort = summary($brandVal["description"], 300);

			        ?>
                    <!-- list view brand item -->
                    <div class="brandlisting <?php echo $activeListing;?>">
                        <div class="row">
                            <div class="col-xs-3 brandlisting-image" >
                                <a href="<?php echo $brandVal['main'][0]['url']?>" target="_blank"><img src="<?php echo $brandVal['logoUrl'];?>" class="img-responsive "></a>
                            </div>
                            <div class="col-xs-5"><p><?php echo $descriptionShort;?></p></div>
                            <div class="col-xs-4 row">
						        <?php foreach ($productTypes['productTypes'] as $types) {

							        if ($brandVal[$types['name']][0]['url']){$visibility = 'visible';}else{ $visibility = 'hidden';}
							        ?>
                                    <div data-toggle="tooltip" title="<?php echo $types['title'];?>" class="icons <?php echo $visibility;?>">
                                        <div class="col-xs-4 col-sm-3 col-md-2"><a href="<?php echo $brandVal[$types['name']][0]['url']?>" target="_blank"> <img src="assets/Product_Icon_<?php echo
										        $types['icon'];?>.png" class="img-responsive " /></a>

                                        </div>
                                    </div>

							        <?php
						        }?>
                            </div>

                        </div>
                    </div>
                    <!-- list view -->
                    <!-- gallery view -->
                    <div class="brandlisting-gallery-container col-sm-6 col-md-4 col-lg-4 col-xs-12 <?php echo $activeListing;?>">
                        <div class="brandlisting-gallery">
                            <div class="row "  >
                                <div class="col-xs-12 brandlisting-image">
                                    <a href="<?php echo $brandVal['main'][0]['url'];?>" target="_blank"><img src="<?php echo $brandVal['logoUrl'];?>" class="img-responsive "></a>
                                </div>
                            </div>
                            <div class="row brandlisting-gallery-description">
                                <div class="col-xs-12">
                                    <p class="description-limit"><?php echo $descriptionShort;?></p>
                                </div>
                            </div>
                            <div class="row brandlisting-gallery-icons">
						        <?php
						        foreach ($productTypes['productTypes'] as $types) {
							        if ($brandVal[$types['name']][0]['url']){$visibility = 'visible';}else{ $visibility = 'hidden';}
							        ?>
                                    <div data-toggle="tooltip" title="<?php echo $types['title'];?>" class="icons <?php echo $visibility;?>">
                                        <div class="col-xs-2 col-sm-2 col-md-2">
                                            <a href="<?php echo $brandVal[$types['name']][0]['url']?>" target="_blank"> <img src="assets/Product_Icon_<?php echo
										        $types['icon'];?>.png" class="img-responsive " /></a>
                                        </div>

                                    </div>

							        <?php
						        }?>
                            </div>
                        </div>

                    </div>
                    <!-- gallery view -->

			        <?php
		        }
		        //end product loop
	        }
	        ?>
        </div>

                <!-- list view -->
                <!-- gallery view -->

        <script>

            $('#layout-selectors i.fa').click( function(){

                var viewOption = $(this).attr('id');
                if (viewOption == "list-view-selector"){
                    $('.brandlisting').css('display','block');
                    $('.brandlisting-gallery-container').css('display','none');


                }else if(viewOption == "grid-view-selector"){
                    $('.brandlisting-gallery-container').css('display','block');
                    $('.brandlisting').css('display','none');
                }

            });

/* When the user clicks on the button,
 toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("countryDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {

        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}





        </script>

		</div>
<!--		Brand and Offering List End-->
	</div>
</div>
