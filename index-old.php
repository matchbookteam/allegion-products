<?php
include ('header.php');
include ('billboard.php');
include ('jsonImport.php');?>
<div class="parbase multipleColumns section">
	<div class="content-container no-margin row">
<!--		Icon Key Section Start-->
		<div  class="icon-key ">
			<div class="icon-key-header">
                <div class="row">
                    <div class="col-xs-8">
                        <h2>Product Icon Key</h2>
                    </div>
                    <div id="layout-selectors" class="col-xs-4">
                        <i id="list-view-selector" class="fa fa-th-list" aria-hidden="true"></i>
                        <i id="grid-view-selector" class="fa fa-th-large" aria-hidden="true"></i>

                    </div>
                </div>
            </div>
			<div class="icon-key-body">
				<div class="row">
					<div id="locks" class="col-xs-4 col-sm-2 col-md-2 selector">
						<div class="row">
							<div class="col-xs-5">
								<img src="assets/Product_Icon_Keys.png" class="img-responsive">
							</div>
						</div>
						<h2>Locks, keys &amp; levers</h2>
						<p>Cylindrical, mortise and tubular locks to deadbolts, levers, knobs and master key systems.</p>
					</div>
					<div id="portable" class="col-xs-4 col-sm-2 col-md-2 selector">
						<div class="row">
							<div class="col-xs-5">
								<img src="assets/Product_Icon_Bike.png" class="img-responsive">
							</div>
						</div>
						<h2>Portable &amp; out of home</h2>
						<p>Cylindrical, mortise and tubular locks to deadbolts, levers, knobs and master key systems.</p>
					</div>
					<div id="electronic" class="col-xs-4 col-sm-2 col-md-2 selector">
						<div class="row">
							<div class="col-xs-5">
								<img src="assets/Product_Icon_Phone.png" class="img-responsive">
							</div>
						</div>
						<h2>Electronic access &amp; monitoring</h2>
						<p>Cylindrical, mortise and tubular locks to deadbolts, levers, knobs and master key systems.</p>
					</div>
					<div id="doors" class="col-xs-4 col-sm-2 col-md-2 selector">
						<div class="row">
							<div class="col-xs-5">
								<img src="assets/Product_Icon_Door.png" class="img-responsive">
							</div>
						</div>
						<h2>Exits, openers, closers &amp; doors</h2>
						<p>Cylindrical, mortise and tubular locks to deadbolts, levers, knobs and master key systems.</p>
					</div>
					<div id="other" class="col-xs-4 col-sm-2 col-md-2 selector">
						<div class="row">
							<div class="col-xs-5">
								<img src="assets/Product_Icon_Hinge.png" class="img-responsive">
							</div>
						</div>
						<h2>Other door hardware &amp; accessories</h2>
						<p>Cylindrical, mortise and tubular locks to deadbolts, levers, knobs and master key systems.</p>
					</div>
					<div id="accessibility" class="col-xs-4 col-sm-2 col-md-2 selector">
						<div class="row">
							<div class="col-xs-5">
								<img src="assets/Product_Icon_Cross.png" class="img-responsive">
							</div>
						</div>
						<h2>Quiet solutions, accessibility &amp; wellness</h2>
						<p>Cylindrical, mortise and tubular locks to deadbolts, levers, knobs and master key systems.</p>
					</div>

				</div>
			</div>
		</div>
<!--		Icon Key Section Stop-->
<!--		Brand and Offering List Start-->
        <div id="brands">
            <div id="active-brands">
            <div class="brandlisting brandlisting-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <h3>Brand</h3>
                    </div>
                    <div class="col-xs-5"><h3>Description</h3></div>
                    <div class="col-xs-4 row"><h3>Offerings</h3>
                    </div>

                </div>
            </div>


	<?php
	$selected = 'other';
	foreach ($brands['brands'] as $brandVal){

		//Sample variable for selected items from sort above

        //redraw listing area based on sort option

        //todo: clear active and inactive listing divs
	    if($brandVal[$selected]){
	        $activeListing = 'active-listing';
	        //todo: jquery add to active listing area
        }else{
		    $activeListing = 'inactive-listing';
		    //todo: jquery add to inactive listing area
        }


?>
        <!-- list view brand item -->
			<div class="brandlisting <?php echo $activeListing;?>">
                <div class="row">
                    <div class="col-xs-3 brandlisting-image" >
                        <img src="<?php echo $brandVal['logoUrl'];?>" class="img-responsive">
                    </div>
                    <div class="col-xs-5"><p><?php echo $brandVal['description'];?></p></div>
                    <div class="col-xs-4 row">
                        <?php foreach ($productTypes['productTypes'] as $types) {

                            if ($brandVal[$types['name']]){$visibility = 'visible';}else{ $visibility = 'hidden';}
                            ?>
                            <div class="icons <?php echo $visibility;?>">
                            <div class="col-xs-4 col-sm-3 col-md-2"><img src="assets/Product_Icon_<?php echo
				$types['icon'];?>.png" class="img-responsive"/> </div>
                            </div>

                        <?php
							}?>
                    </div>

				</div>
			</div>
        <!-- list view -->
        <!-- gallery view -->
        <div class="brandlisting-gallery-container col-sm-6 col-md-4 col-lg-4 col-xs-12 <?php echo $activeListing;?>">
            <div class="brandlisting-gallery">
            <div class="row "  >
                <div class="col-xs-12 brandlisting-image">
                    <img src="<?php echo $brandVal['logoUrl'];?>" class="img-responsive ">
                </div>
            </div>
            <div class="row brandlisting-gallery-description">
                <div class="col-xs-12">
                    <p><?php echo $brandVal['description'];?></p>
                </div>
            </div>
            <div class="row brandlisting-gallery-icons">
            <?php
            foreach ($productTypes['productTypes'] as $types) {
                if ($brandVal[$types['name']]){$visibility = 'visible';}else{ $visibility = 'hidden';}
            ?>
                    <div class="icons  <?php echo $visibility;?>">
                        <div class="col-xs-2 col-sm-2 col-md-2">
                            <img src="assets/Product_Icon_<?php echo $types['icon'];?>.png" class="img-responsive"/>
                        </div>
                    </div>

            <?php
            }?>
            </div>
            </div>

        </div>
        <!-- gallery view -->

		<?php

	}
	?>
            </div>

            <div id="inactive-brands">
                <!-- gallery view -->
                <div class="brandlisting-gallery-container col-sm-6 col-md-4 col-lg-4 col-xs-12 <?php echo $activeListing;?>">
                    <div class="brandlisting-gallery">
                        <div class="row brandlisting-gallery-image"  >
                            <div class="col-xs-12 ">
                                <img src="<?php echo $brandVal['logoUrl'];?>" class="img-responsive ">
                            </div>
                        </div>
                        <div class="row brandlisting-gallery-description">
                            <div class="col-xs-12">
                                <p><?php echo $brandVal['description'];?></p>
                            </div>
                        </div>
                        <div class="row brandlisting-gallery-icons">
                            <?php
                            foreach ($productTypes['productTypes'] as $types) {
                                if ($brandVal[$types['name']]){$visibility = 'visible';}else{ $visibility = 'hidden';}
                                ?>
                                <div class="icons  <?php echo $visibility;?>">
                                    <div class="col-xs-2 col-sm-2 col-md-2">
                                        <img src="assets/Product_Icon_<?php echo $types['icon'];?>.png" class="img-responsive"/>
                                    </div>
                                </div>

                                <?php
                            }?>
                        </div>
                    </div>

                </div>
                <!-- gallery view -->
                <!-- list view -->
                <div class="brandlisting brandlisting-heading">
                    <div class="row">
                        <div class="col-xs-3">
                             <h3>Brand</h3>
                        </div>
                        <div class="col-xs-5">
                             <h3>Description</h3>
                         </div>
                         <div class="col-xs-4 row">
                             <h3>Offerings</h3>
                         </div>
                    </div>
                </div>

                <!-- todo: setup with jquery -->
                <div class="brandlisting <?php echo $activeListing;?>">
                    <div class="row">
                        <div class="col-xs-3">
                            <img src="<?php echo $brandVal['logoUrl'];?>" class="img-responsive">
                        </div>
                        <div class="col-xs-5"><p><?php echo $brandVal['description'];?></p></div>
                        <div class="col-xs-4 row">
                            <?php foreach ($productTypes['productTypes'] as $types) {

                                if ($brandVal[$types['name']]){$visibility = 'visible';}else{ $visibility = 'hidden';}
                                ?>
                                <div class="icons <?php echo $visibility;?>">
                                    <div class="col-xs-4 col-sm-3 col-md-2"><img src="assets/Product_Icon_<?php echo
                                        $types['icon'];?>.png" class="img-responsive"/> </div>
                                </div>

                                <?php
                            }?>
                        </div>

                    </div>
                </div>

                <!-- list view -->

            </div>


    </div>
        <script>

            $('#layout-selectors i.fa').click( function(){

                var viewOption = $(this).attr('id');
                if (viewOption == "list-view-selector"){
                    $('.brandlisting').css('display','block');
                    $('.brandlisting-gallery-container').css('display','none');


                }else if(viewOption == "grid-view-selector"){
                    $('.brandlisting-gallery-container').css('display','block');
                    $('.brandlisting').css('display','none');
                }

            });


            $('.selector').click( sortBrands());
            function sortBrands(){
                //set active items
//                var $activeItemsVar = $('.inactive-listing');
                $selection = this.attr("id");
                $('#results').html($selection);

            }




        </script>

		</div>
<!--		Brand and Offering List End-->
	</div>
</div>
