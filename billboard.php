	<div class="content parsys"><div class="parbase billboard multipleColumns section">

		<div class="billboard ">
			<div class="container" style="min-height: 400px; background-image: url('/allegion/assets/Product_Banner_Image.jpg');">
				<div class="col-xs-12 col-sm-5 col-md-3 caption" style="background-color: rgba(255, 103, 31, 0.75);">

					<h1  style="color: #ffffff">Our Products</h1>

					<div class="description" style="color: #ffffff">
						<p>Specializing in security around the door, we furnish locks (both mechanical and electrical), door closers, exit devices, steel doors and frames. We're also breaking new technological grounds with biometrics, special surfaces, video surveillance, wireless connectivity, cloud technology and other electronic safety solutions.</p>



                            <div class="dropdown">
                                <button type="button" onclick="myFunction()" class="dropbtn btn btn-primary">Country <span class="caret"></span></i></button>
                                <div id="countryDropdown" class="dropdown-content">


                                   <?php
	                                    foreach ($countries['countries'] as $country){
		                                    ?>
                                    <a href="#">
                                        <?php echo($country['name']); ?>
                                    </a>
	                                 <?php   }?>

                                </div>
                        </div>

					</div>

				</div>

				<div class="col-xs-12 picture" style="background-image: url('/allegion/assets/Product_Banner_Image.jpg');"></div>
			</div>
		</div>



	</div>
